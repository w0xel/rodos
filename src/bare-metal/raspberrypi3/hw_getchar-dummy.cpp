#include <rodos.h>

namespace RODOS {

char  getcharNoWait() { return 0; }
bool  isCharReady() { return false; }
char* getsNoWait() { return 0; }
void  activateTopicCharInput() {}

} // namespace RODOS
