RODOS RODOS-200.0 OS Version Linux-x86
Loaded Applications:
         10 -> 'Topics & Middleware'
       1000 -> 'terminateTest'
       1000 -> 'TestTimeAT'
Calling Initiators and Application Initiators
 initiator nodnr = 7
Distribute Subscribers to Topics
List of Middleware Topics:
 CharInput  Id = 28449 len = 16.   -- Subscribers:
 SigTermInterrupt  Id = 16716 len = 4.   -- Subscribers:
 UartInterrupt  Id = 15678 len = 4.   -- Subscribers:
 TimerInterrupt  Id = 25697 len = 4.   -- Subscribers:
 routerTopic  Id = 21324 len = 1326.   -- Subscribers:
 gatewayTopic  Id = 0 len = 16.   -- Subscribers:

Event servers:
Threads in System:
   Prio =       0 Stack =  32000 IdleThread: yields all the time
   Prio =    1003 Stack =  32000 TerminateTest: 
   Prio =     100 Stack =  32000 waitAT: Waiting time
BigEndianity = 0, cpu-Arc = x86, Basis-Os = baremetal, Cpu-Speed (K-Loops/sec) = 313330 yeildtim (ns) 990
Node Number: HEX: 7 Dec: 7
-----------------------------------------------------
Default internal MAIN
--------------- Application running ------------

This run (test) will be terminated in 20 Seconds
waiting until 3rd second after start
after 3rd second
waiting until 1 second has pased
1 second has pased
print every 2 seconds, start at 5 seconds
current time:   0.100000000
current time:   0.200000000
current time:   0.300000000
current time:   0.400000000
current time:   0.500000000
current time:   0.600000000
current time:   0.700000000
current time:   0.800000000
current time:   0.900000000
current time:   1.000000000
current time:   1.100000000
current time:   1.199999999
current time:   1.300000000
current time:   1.399999999
current time:   1.500000000
current time:   1.600000000
current time:   1.700000000
current time:   1.800000000
current time:   1.900000000
current time:   2.000000000
current time:   2.100000000
current time:   2.200000000
current time:   2.299999999
current time:   2.399999999
current time:   2.500000000
current time:   2.600000000
current time:   2.700000000
current time:   2.799999999
current time:   2.900000000
current time:   3.000000000
current time:   3.100000000
current time:   3.200000000
current time:   3.299999999
current time:   3.399999999
current time:   3.500000000
current time:   3.600000000
current time:   3.700000000
current time:   3.799999999
current time:   3.900000000
current time:   4.000000000
current time:   4.099999999
current time:   4.200000000
current time:   4.299999999
current time:   4.400000000
current time:   4.500000000
current time:   4.599999999
current time:   4.700000000
current time:   4.799999999
current time:   4.900000000
current time:   5.000000000
current time:   5.099999999
current time:   5.200000000
current time:   5.299999999
current time:   5.400000000
current time:   5.500000000
current time:   5.599999999
current time:   5.700000000
current time:   5.799999999
current time:   5.900000000
current time:   6.000000000
current time:   6.099999999
current time:   6.200000000
current time:   6.299999999
current time:   6.400000000
current time:   6.500000000
current time:   6.599999999
current time:   6.700000000
current time:   6.799999999
current time:   6.900000000
current time:   7.000000000
current time:   7.099999999
current time:   7.200000000
current time:   7.299999999
current time:   7.400000000
current time:   7.500000000
current time:   7.599999999
current time:   7.700000000
current time:   7.799999999
current time:   7.900000000
current time:   8.000000000
current time:   8.099999999
current time:   8.199999999
current time:   8.300000000
current time:   8.400000000
current time:   8.500000000

This run (test) terminates now! but first error log:
time(secs)         id  text/name
---------------------------------

This run (test) terminates now! (terminate-test.cpp : 20)
