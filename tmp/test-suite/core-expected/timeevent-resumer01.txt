RODOS RODOS-200.0 OS Version Linux-x86
Loaded Applications:
         10 -> 'Topics & Middleware'
       1000 -> 'terminateTest'
       1000 -> 'TestTimebeats & events 01'
Calling Initiators and Application Initiators
 initiator nodnr = 7
Distribute Subscribers to Topics
List of Middleware Topics:
 CharInput  Id = 28449 len = 16.   -- Subscribers:
 SigTermInterrupt  Id = 16716 len = 4.   -- Subscribers:
 UartInterrupt  Id = 15678 len = 4.   -- Subscribers:
 TimerInterrupt  Id = 25697 len = 4.   -- Subscribers:
 routerTopic  Id = 21324 len = 1326.   -- Subscribers:
 gatewayTopic  Id = 0 len = 16.   -- Subscribers:

Event servers:
	1 TimeEvent managers
Threads in System:
   Prio =       0 Stack =  32000 IdleThread: yields all the time
   Prio =    1003 Stack =  32000 TerminateTest: 
   Prio =     100 Stack =  32000 AnonymThread: 
BigEndianity = 0, cpu-Arc = x86, Basis-Os = baremetal, Cpu-Speed (K-Loops/sec) = 313330 yeildtim (ns) 990
Node Number: HEX: 7 Dec: 7
-----------------------------------------------------
Default internal MAIN
--------------- Application running ------------

This run (test) will be terminated in 20 Seconds
First beat in 5 seconds, period 5 seconds
in 5 seconds beat: 5 
Time Event at 10 -> resuming
in 5 seconds beat: 10 
Time Event at 12 -> resuming
in 5 seconds beat: 12 
Time Event at 14 -> resuming
in 5 seconds beat: 14 
in 5 seconds beat: 15 
Time Event at 16 -> resuming
in 5 seconds beat: 16 
Time Event at 18 -> resuming
in 5 seconds beat: 18 

This run (test) terminates now! but first error log:
time(secs)         id  text/name
---------------------------------

This run (test) terminates now! (terminate-test.cpp : 20)
