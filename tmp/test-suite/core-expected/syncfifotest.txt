RODOS RODOS-200.0 OS Version Linux-x86
Loaded Applications:
         10 -> 'Topics & Middleware'
       1000 -> 'terminateTest'
Calling Initiators and Application Initiators
 initiator nodnr = 7
Distribute Subscribers to Topics
List of Middleware Topics:
 CharInput  Id = 28449 len = 16.   -- Subscribers:
 SigTermInterrupt  Id = 16716 len = 4.   -- Subscribers:
 UartInterrupt  Id = 15678 len = 4.   -- Subscribers:
 TimerInterrupt  Id = 25697 len = 4.   -- Subscribers:
 routerTopic  Id = 21324 len = 1326.   -- Subscribers:
 gatewayTopic  Id = 0 len = 16.   -- Subscribers:

Event servers:
Threads in System:
   Prio =       0 Stack =  32000 IdleThread: yields all the time
   Prio =    1003 Stack =  32000 TerminateTest: 
   Prio =     100 Stack =  32000 AnonymThread: 
   Prio =     100 Stack =  32000 AnonymThread: 
BigEndianity = 0, cpu-Arc = x86, Basis-Os = baremetal, Cpu-Speed (K-Loops/sec) = 313330 yeildtim (ns) 990
Node Number: HEX: 7 Dec: 7
-----------------------------------------------------
Default internal MAIN
--------------- Application running ------------

This run (test) will be terminated in 20 Seconds
sent 1
sent 2
sent 3
sent 4
sent 5
sent 6
sent 7
sent 8
sent 9
read 1
read 2
read 3
read 4
read 5
read 6
read 7
read 8
read 9
sent 10
sent 11
sent 12
sent 13
sent 14
sent 15
sent 16
sent 17
sent 18
read 10
read 11
read 12
read 13
read 14
read 15
read 16
read 17
read 18
sent 19
sent 20
Sender Wainting 3 seconds
read 19
read 20
sent 21
sent 22
sent 23
sent 24
sent 25
sent 26
sent 27
sent 28
sent 29
read 21
read 22
read 23
read 24
read 25
read 26
read 27
read 28
read 29
sent 30
sent 31
sent 32
sent 33
sent 34
sent 35
sent 36
sent 37
sent 38
read 30
read 31
read 32
read 33
read 34
read 35
read 36
read 37
read 38
sent 39
sent 40
Sender Wainting 3 seconds
read 39
read 40
sent 41
sent 42
sent 43
sent 44
sent 45
sent 46
sent 47
sent 48
sent 49
read 41
read 42
read 43
read 44
read 45
read 46
read 47
read 48
read 49
sent 50
sent 51
sent 52
sent 53
sent 54
sent 55
sent 56
sent 57
sent 58
read 50
Reciver wait 15 seconds
sent 59

This run (test) terminates now! but first error log:
time(secs)         id  text/name
---------------------------------

This run (test) terminates now! (terminate-test.cpp : 20)
