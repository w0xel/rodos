RODOS RODOS-200.0 OS Version Linux-x86
Loaded Applications:
         10 -> 'Topics & Middleware'
       1000 -> 'terminateTest'
       1000 -> 'TestTimeAT'
Calling Initiators and Application Initiators
 initiator nodnr = 7
Distribute Subscribers to Topics
List of Middleware Topics:
 CharInput  Id = 28449 len = 16.   -- Subscribers:
 SigTermInterrupt  Id = 16716 len = 4.   -- Subscribers:
 UartInterrupt  Id = 15678 len = 4.   -- Subscribers:
 TimerInterrupt  Id = 25697 len = 4.   -- Subscribers:
 routerTopic  Id = 21324 len = 1326.   -- Subscribers:
 gatewayTopic  Id = 0 len = 16.   -- Subscribers:

Event servers:
Threads in System:
   Prio =       0 Stack =  32000 IdleThread: yields all the time
   Prio =    1003 Stack =  32000 TerminateTest: 
   Prio =     100 Stack =  32000 waitAT: Waiting 2 seconds
BigEndianity = 0, cpu-Arc = x86, Basis-Os = baremetal, Cpu-Speed (K-Loops/sec) = 313330 yeildtim (ns) 990
Node Number: HEX: 7 Dec: 7
-----------------------------------------------------
Default internal MAIN
--------------- Application running ------------

This run (test) will be terminated in 20 Seconds
Starting and waiting until 3th second
after 3th second
After 2 Seconds  :   0.100000000 1
After 2 Seconds  :   0.200000000 2
After 2 Seconds  :   0.300000000 3
After 2 Seconds  :   0.400000000 4
After 2 Seconds  :   0.500000000 5
After 2 Seconds  :   0.600000000 6
After 2 Seconds  :   0.700000000 7
After 2 Seconds  :   0.800000000 8

This run (test) terminates now! but first error log:
time(secs)         id  text/name
---------------------------------

This run (test) terminates now! (terminate-test.cpp : 20)
